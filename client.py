import socket
import threading
################################################
class Template(object):
    def __getitem__(self, key):
        return getattr(self, str(key))
    def __setitem__(self, key, value):
        return setattr(self, str(key), value)
################################################

PREFIX = Template()
PREFIX.RESULT, PREFIX.STATUS, PREFIX.MONITOR = 'result', 'status', 'monitor'

LOCK = Template()
LOCK.SEND, LOCK.PRINT, LOCK.BUFFER = [threading.Lock() for i in range(3)]

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 4455        # The port used by the server

list_threads = []
BUFFER = []


def listening_thread(sock):
    while True:
        data = sock.recv(1024).decode('UTF-8')

        if data.startswith(PREFIX.STATUS):
            LOCK.PRINT.acquire() 
            try:
                print(data[8:])
            except:
                print('Too much status to post')
            LOCK.PRINT.release()

        elif data.startswith(PREFIX.MONITOR):
            LOCK.PRINT.acquire() 
            try:
                print(data[11:])
            except:
                print('Too much thread to monitor')
            LOCK.PRINT.release()
        
        else:
            LOCK.BUFFER.acquire()
            BUFFER.append(data.split(':'))
            LOCK.BUFFER.release()

def client_thread(sock, data_input):
    LOCK.SEND.acquire()
    sock.send(data_input)
    LOCK.SEND.release()

def print_buffer():
    LOCK.BUFFER.acquire()
    LOCK.PRINT.acquire()
    try:
        print("\n".join(list(map(lambda x: f"{x[1]} = {x[2]}", BUFFER))))
    except:
        print('Output too long')
    LOCK.PRINT.release()
    BUFFER.clear()
    
    LOCK.BUFFER.release()

def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

        s.connect((HOST, PORT))

        listen_thread = threading.Thread(
            target=listening_thread, 
            args=(s, ), 
            daemon=True
        ).start()

        print(f'{HOST} Connected')
        while True:
            data_input = input().lower()

            if data_input == 'output()': print_buffer()
            elif data_input == 'exit()': break
            elif data_input == '--help': print(f'Return number that is inputted\noutput()\nexit()\nmonitor()')
            else:
                thread_alive, thread_dead = [], []
                [thread_alive.append(thread) if thread.is_alive() else thread_dead.append(thread) for thread in list_threads]
                [thread.join for thread in thread_dead]
            
                threads = thread_alive
                
                x = threading.Thread(
                    target=client_thread, 
                    args=(s , str.encode(data_input)), 
                    daemon=True
                )

                threads.append(x)
                x.start()
        
if __name__ == "__main__":          
    main()