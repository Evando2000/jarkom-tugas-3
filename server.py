from queue import Queue
import socket
import threading
import time
################################################
class Template(object):
    def __getitem__(self, key):
        return getattr(self, str(key))
    def __setitem__(self, key, value):
        return setattr(self, str(key), value)
################################################

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 4455        # Port to listen on (non-privileged ports are > 1023)

LOCK = threading.Lock()

THREAD_COUNT = 3

INACTIVE_THREAD_LIMIT = 5
QUEUE_LIMIT = 3
THREAD_KILL_DIVIDE = 2

STATUS = Template()
STATUS.WAITING, STATUS.RUNNING, STATUS.FAILED, STATUS.FINISHED = 'Waiting', 'Running', 'Failed', 'Finished'

THREAD = Template()
THREAD.ACTIVE, THREAD.DEAD, THREAD.RUNNING = 'Active', 'Dead', 'Running'

TIME = Template()
TIME.FAST, TIME.AVERAGE, TIME.SLOW = 'Fast', 'Average', 'Slow'

PREFIX = Template()
PREFIX.RESULT, PREFIX.STATUS, PREFIX.MONITOR = 'result', 'status', 'monitor()'

ALL_THREADS = []

# Fast speed job
def lazy_sum(numbers):
    time.sleep(0.1)
    return int(numbers[0]) + int(numbers[1])

# Average speed job
def lazy_fibonacci(fibo_count):
    time.sleep(0.1)
    num1 = 0
    num2 = 1
    count = 0
    list_fibo = []
    fibo_count = int(fibo_count[0])

    if fibo_count <= 1:
        list_fibo.append(0)
        list_fibo.append(1)
        return str(list_fibo)

    else :
        while count < fibo_count :
            time.sleep(0.1)
            list_fibo.append(num1)
            nth = num1 + num2
            num1 = num2
            num2 = nth
            count += 1
        return str(list_fibo)

# Slow speed job
def lazy_bubbleSort(arr): 
    length_arr = len(arr)
    arr = [eval(x) for x in arr]
    for i in range(length_arr-1): 
        for j in range(0, length_arr-i-1): 
            time.sleep(0.1)
            if arr[j] > arr[j+1] : 
                arr[j], arr[j+1] = arr[j+1], arr[j]
    return str(arr)

JOB = Template()
JOB.SUM, JOB.FIB, JOB.SORT = 'sum', 'fib', 'sort'
JOBS = {
    JOB.SUM : lazy_sum,
    JOB.FIB : lazy_fibonacci,
    JOB.SORT : lazy_bubbleSort
}

class MyThread(threading.Thread):
    def __init__(self, queue, args=(), kwargs=None):
        threading.Thread.__init__(self, args=(), kwargs=None)
        self.thread_name = ''
        self.job_name = ''

        self.job_status = STATUS.WAITING
        self.thread_status = THREAD.ACTIVE

        self.job_history = {}
        self.queue = queue

        self._stop_event = threading.Event()

    def run(self):
        while True:
            if self.thread_status == THREAD.DEAD: break

            self.thread_name = threading.currentThread().getName().replace('Thread', 'Worker')

            self.job_status = STATUS.WAITING
            self.thread_status = THREAD.ACTIVE

            data, connection = self.queue.get()

            self.job_name = data
            temp_job_data = data.split()

            self.job_status = STATUS.RUNNING
            self.job_history[self.job_name] = self.job_status
            self.thread_status = THREAD.RUNNING

            print(f'{self.thread_name}: Job {self.job_name}, {self.job_status}')
            start = 0
            try:
                start = time.time()
           
                result = JOBS[temp_job_data[0]](temp_job_data[1:])

                self.send(PREFIX.RESULT, result, connection)
                self.job_status = STATUS.FINISHED

            except Exception as e:
                self.job_status = STATUS.FAILED

            finally:
                end = time.time()
                self.job_history[self.job_name] = self.job_status

                time_taken = end - start
                if time_taken <= 1: time_taken = TIME.FAST
                elif 1 < time_taken <= 5: time_taken = TIME.AVERAGE
                else: time_taken = TIME.SLOW

                print(f'{self.thread_name}: Job {self.job_name}, {self.job_status}, {time_taken}, {end - start:.2f} seconds elapsed')

    def stop(self):
        self._stop_event.set()
        self.thread_status = THREAD.DEAD
        
    def is_stopped(self):
        return self._stop_event.is_set()

    #* Mutex Implementation
    def send(self, send_type, data, connection):
        LOCK.acquire()
        try:
            if data:
                if send_type == PREFIX.RESULT : connection.send(bytes(f'{PREFIX.RESULT}:{self.job_name}:{data}', 'UTF-8'))
        finally:
            LOCK.release()

def make_thread():
    queue = Queue()
    thread = MyThread(queue)
    thread.start()
    time.sleep(0.1)

    return thread
        
def distribute(connection) -> None:
    while True:
        data = connection.recv(1024).decode('UTF-8')
        if not data: break

        if data.startswith(PREFIX.STATUS):
            res = ''
            for thread in ALL_THREADS:
                for job in thread.job_history:
                    if job.split(' ')[0] == data.split()[1]: 
                        res += f'{PREFIX.STATUS} {job}: {thread.job_history[job]}, {thread.thread_name}\n'
            connection.send(bytes(f"{PREFIX.STATUS}, {res or 'No operation has been made'}", 'UTF-8'))

        elif data.startswith(PREFIX.MONITOR):
            res = '\n'.join([f'{thread.thread_name} {thread.thread_status}' for thread in ALL_THREADS])
            connection.send(bytes(f"{PREFIX.MONITOR}, {res or 'No threads to monitor'}", 'UTF-8'))

        #! Check for threads that has the lowest job count
        queue_length = [x.queue.qsize() for x in ALL_THREADS if x.thread_status != THREAD.DEAD]

        # * Upscaling
        if min(queue_length) >= QUEUE_LIMIT: ALL_THREADS.append(make_thread())

        # * Downscaling
        if queue_length.count(0) > len(ALL_THREADS) // 2:
            inactive_threads = list(filter(lambda thread: thread.queue.qsize() == 0, ALL_THREADS))
            if len(inactive_threads) >= INACTIVE_THREAD_LIMIT: [inactive_threads[i].stop() for i in range (len(inactive_threads) // THREAD_KILL_DIVIDE)]

        if not data.startswith(PREFIX.STATUS) and not data.startswith(PREFIX.MONITOR):
            active_threads = [x for x in ALL_THREADS if x.thread_status != THREAD.DEAD]
            queue_length = [x.queue.qsize() for x in active_threads]
            active_threads[queue_length.index(min(queue_length))].queue.put((data, connection))
        
def main():
    print('Server Started')
    [ALL_THREADS.append(make_thread()) for i in range(THREAD_COUNT)]
    while True:        
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

            s.bind((HOST, PORT))
            s.listen()

            connection, address = s.accept()
            with connection:
                print('Connected by', address)
                try:
                    distribute(connection)
                except Exception as e:
                    print(e)
                    print(f'Disonnected by {address}')

if __name__ == "__main__":          
    main()